-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-06-2018 a las 06:47:45
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `consultorionutr`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `adm_email` text NOT NULL,
  `adm_password` varchar(256) NOT NULL,
  `adm_name` text NOT NULL,
  `adm_last_name_1` text NOT NULL,
  `adm_last_name_2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id_admin`, `adm_email`, `adm_password`, `adm_name`, `adm_last_name_1`, `adm_last_name_2`) VALUES
(1, 'admin@gmail.com', 'admin', 'Marcelo', 'Montaño', 'Vidal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appointment`
--

CREATE TABLE `appointment` (
  `id_appointment` int(11) NOT NULL,
  `date_appointment` date NOT NULL,
  `time_appointment` time NOT NULL,
  `id_patient` int(11) NOT NULL,
  `atendido_appointment` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `appointment`
--

INSERT INTO `appointment` (`id_appointment`, `date_appointment`, `time_appointment`, `id_patient`, `atendido_appointment`) VALUES
(2, '2018-06-12', '14:00:00', 3, 2),
(4, '2018-06-15', '14:00:00', 2, 2),
(5, '2018-06-14', '14:00:00', 2, 2),
(7, '2018-06-15', '17:00:00', 3, 2),
(8, '2018-06-15', '14:00:00', 3, 2),
(9, '2018-06-16', '14:00:00', 3, 2),
(10, '2018-06-16', '15:00:00', 3, 1),
(12, '2018-06-16', '16:00:00', 3, 0),
(13, '2018-06-16', '17:00:00', 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `free_prescription`
--

CREATE TABLE `free_prescription` (
  `id_free_prescription` int(11) NOT NULL,
  `content_free_prescription` text NOT NULL,
  `title_free_prescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `free_prescription`
--

INSERT INTO `free_prescription` (`id_free_prescription`, `content_free_prescription`, `title_free_prescription`) VALUES
(10, '<h1>Receta buenona</h1><br><h2>Pasos Basicos</h2><br>Comer menos xd<br><h2></h2><br><br><h2></h2><br><br><h2></h2><br><br><h2></h2><br>', 'Receta buenona'),
(11, '<h1>Para chicos malos</h1> <br><h2>Me gusta esta receta</h2><br>es buenona<br><h2>Este es el titulo 2</h2><br>y el contenido 2<br><h2></h2><br><br><h2></h2><br><br><h2></h2><br>', 'Para chicos malos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patient`
--

CREATE TABLE `patient` (
  `id_patient` int(11) NOT NULL,
  `pat_name` text NOT NULL,
  `pat_last_name_1` text NOT NULL,
  `pat_last_name_2` text NOT NULL,
  `pat_phone` int(11) NOT NULL,
  `pat_direction` text NOT NULL,
  `pat_email` text NOT NULL,
  `pat_password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `patient`
--

INSERT INTO `patient` (`id_patient`, `pat_name`, `pat_last_name_1`, `pat_last_name_2`, `pat_phone`, `pat_direction`, `pat_email`, `pat_password`) VALUES
(2, 'Bernardo', 'Barrancos', 'Contretas', 75491290, 'Por ahi', 'bernardo@gmail.com', 'bernardinho'),
(3, 'Luis', 'Arevalo', 'Antezana', 79339707, 'Por el Maryknoll', 'Lucho@gmail.com', 'lucho');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pat_prescription`
--

CREATE TABLE `pat_prescription` (
  `id_pat_prescription` int(11) NOT NULL,
  `content_pat_prescription` text NOT NULL,
  `id_patient` int(11) NOT NULL,
  `date_pat_prescription` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pat_prescription`
--

INSERT INTO `pat_prescription` (`id_pat_prescription`, `content_pat_prescription`, `id_patient`, `date_pat_prescription`) VALUES
(5, 'El berno Buenon', 2, '0000-00-00'),
(6, 'El berno Buenon', 2, '0000-00-00'),
(7, 'El berno Buenon', 2, '0000-00-00'),
(8, 'El berno Buenon', 2, '0000-00-00'),
(9, 'El berno Buenon', 2, '0000-00-00'),
(10, 'asd', 2, '0000-00-00'),
(11, 'Lucho Pucho', 3, '2018-06-12'),
(12, 'Lucho estas loco xddDDddDdd', 3, '2018-06-14'),
(13, 'Esta es la ultima receta que te asignare en tu vida >:v', 3, '2018-06-15');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id_appointment`),
  ADD KEY `id_patient` (`id_patient`);

--
-- Indices de la tabla `free_prescription`
--
ALTER TABLE `free_prescription`
  ADD PRIMARY KEY (`id_free_prescription`);

--
-- Indices de la tabla `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id_patient`);

--
-- Indices de la tabla `pat_prescription`
--
ALTER TABLE `pat_prescription`
  ADD PRIMARY KEY (`id_pat_prescription`),
  ADD KEY `id_patient` (`id_patient`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id_appointment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `free_prescription`
--
ALTER TABLE `free_prescription`
  MODIFY `id_free_prescription` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `patient`
--
ALTER TABLE `patient`
  MODIFY `id_patient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pat_prescription`
--
ALTER TABLE `pat_prescription`
  MODIFY `id_pat_prescription` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`id_patient`) REFERENCES `patient` (`id_patient`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `pat_prescription`
--
ALTER TABLE `pat_prescription`
  ADD CONSTRAINT `pat_prescription_ibfk_1` FOREIGN KEY (`id_patient`) REFERENCES `patient` (`id_patient`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
