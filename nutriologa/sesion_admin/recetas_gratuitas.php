<?php
session_start();
if(isset($_SESSION['a_nombre'])){ ?>
<?php
require("../includes/conexion.php"); 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Recetas gratuitas</title>
</head>

<body style="background: url(../imagenes/recetas_gratuitas.jpg);background-repeat: no-repeat;background-attachment: fixed">
    <header>
        <nav class="navbar navbar-dark " style="background-color: #e74922;">
            <h3>Recetas Gratuitas</h3>
        </nav>
    </header>
    <div style=" background: rgba(0, 0, 0, 0.404);padding-top: 50px;padding-bottom: 50px;">
        <div class="container" style="color: white">



            <?php
         $sql= "SELECT * FROM free_prescription";
         $result=mysqli_query($conn,$sql) or die ('No se ejecuto la consulta');
         $resultcheck=mysqli_num_rows($result);
         if($resultcheck<1)
         {
             echo 'No se encontraron recetas gratuitas';
         }
         else {            
             while ($row=mysqli_fetch_array($result))
             {                
                 $contenido = $row[1];
                 echo $contenido;      
                 ?>
                  <hr class="featurette-divider" style="background: white">
                 <?php
                 
             }
         }     
    ?>
        </div>
    </div>
</body>

</html>
<?php }else {
header("Location: ../inicio.php");
}
?>