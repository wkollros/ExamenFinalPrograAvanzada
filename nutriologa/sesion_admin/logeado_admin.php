<?php
session_start();
require("../includes/conexion.php");  
?>
<?php
if(isset($_SESSION['a_nombre'])){ ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Principal_admin</title>
</head>

<body style="background: url(../imagenes/fondo_admin.jpg); background-repeat: no-repeat;
    background-attachment: fixed">
    <header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: #e08814;">
            <h1 class="navbar-brand">Administrador</h1>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <!-- <li class="nav-item" style="font-size: large">                        
                        <a class="nav-link" href="Crear_receta_gratuita.php">Crear receta gratuita</a> 
                    </li> -->
                    <li class="nav-item " style="font-size: large">                        
                    <a class="nav-link font-weight-bold" href="recetas_gratuitas.php">Recetas Gratuitas</a> 
                    </li>
                    <li class="nav-item" style="font-size: large">                        
                    <a class="nav-link font-weight-bold" href="Crear_receta_cliente.php">Crear receta para cliente</a> 
                    </li>
                    <li class="nav-item" style="font-size: large">                        
                    <a  class="nav-link font-weight-bold" href="Crear_receta_gratuita2.php">Crear Receta Gratuita</a>  
                    </li>                   
                </ul>               
            </div>
            <?php 
                    if(isset($_SESSION['a_nombre'])){
                         echo '<form action="logout-inc.php" method="POST">            
                                  <button class="btn btn-info" type="submit" style="margin-left: 5px" name="submit">Cerrar Sesion</button>
                               </form>';                    
                    }
            ?>
        </nav>


    </header>
    <!-- <a href="Crear_receta_gratuita.php">Crear receta gratuita</a> 
    <a href="recetas_gratuitas.php">Ver recetas gratuitas</a> 
    <a href="Crear_receta_cliente.php">Crear receta para cliente</a> 
    <a href="Crear_receta_gratuita2.php">Crear_receta_gratuita2</a>    -->
                        
    <br><br><br><br>
    <h2 class="font-italic">Citas pendientes</h2>
    <form method="POST" action="proceso.php">
        <div>
            <?php                        
                $sql= "SELECT * FROM appointment WHERE atendido_appointment = 0";
                $result=mysqli_query($conn,$sql) or die ('No se ejecuto la consulta');
                $numerocita = 1;
                $resultcheck=mysqli_num_rows($result);                
                if ($resultcheck>0) {                
                ?>
            <table class="table table-striped">
                <thead>
                    <tr class="bg-warning" style="font-size: 20px;">
                        <th scope="col">#</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">Hora</th>
                        <th scope="col">Paciente</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Accion</th>
                    </tr>
                </thead>
                <?php
               
                                          
                $sql= "SELECT * FROM appointment WHERE atendido_appointment = 0";
                $result=mysqli_query($conn,$sql) or die ('No se ejecuto la consulta');
                $numerocita = 1;
                while ($row=mysqli_fetch_array($result))
                {
                    $id_cita = $row[0];
                    $fecha = $row[1];
                    $hora = $row[2];
                    $id_paciente = $row[3];
                    $atendido = $row[4];

                    $result_paciente = mysqli_query($conn,"SELECT * FROM patient WHERE id_patient = '$id_paciente'" );
                    $row_paciente=mysqli_fetch_array($result_paciente);
                    $nombre_paciente = $row_paciente[1];
                    $telefono_paciente = $row_paciente[4];                                                                                     
                    ?>
                <tbody>
                    <tr style="background:  rgba(255, 255, 255, 0.534)">
                        <th scope="row" class="" style="font-size: 20px;background: rgba(241, 253, 76, 0.397)">
                            <?php echo $numerocita;?>
                        </th>
                        <td class="table-success" style="font-size: 20px;background: rgba(255, 255, 255, 0.397)">
                            <?php echo $fecha;?>
                        </td>
                        <td class="table-warning" style="font-size: 20px;background: rgba(255, 255, 255, 0.397)">
                            <?php echo $hora;?>
                        </td>
                        <td class="table-success" style="font-size: 20px;background: rgba(255, 255, 255, 0.397)">
                            <?php echo $nombre_paciente;?>
                        </td>
                        <td class="table-warning" style="font-size: 20px;background: rgba(255, 255, 255, 0.397)">
                            <?php echo $telefono_paciente;?>
                        </td>
                        <td class="table-success" style="font-size: 20px;background: rgba(255, 255, 255, 0.397)">                           
                            <a href="confirmar_cita.php?id_cita=<?php echo $id_cita ?>" class="btn btn-info" onClick="return confirm('Confirmar cita?')">Confirmar cita</a>                           
                            <a href="eliminar_cita.php?id_cita=<?php echo $id_cita ?>" class="btn btn-dark" onClick="return confirm('Realmente desea eliminar la cita?')">Eliminar cita</a>
                        </td>
                    </tr>            
                    <?php $numerocita++;                    
                    } ?>
                </tbody>
            </table>
                <?php } else{
                    echo 'No existen citas pendientes de confirmacion';
                }
                ?>
        </div>
    </form>
    <h2 class="font-italic">Citas Confirmadas</h2>
    <form method="POST">
        <div>
        <?php                        
                $sql= "SELECT * FROM appointment WHERE atendido_appointment = 1";
                $result=mysqli_query($conn,$sql) or die ('No se ejecuto la consulta');
                $numerocita = 1;
                $resultcheck=mysqli_num_rows($result);                
                if ($resultcheck>0) {                
                ?>
                <table class="table table-striped">
                    <thead>
                        <tr class="bg-warning" style="font-size: 20px;">
                            <th scope="col">#</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Hora</th>
                            <th scope="col">Paciente</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Accion</th>
                        </tr>
                    </thead>
                    <?php                                              
                    $sql= "SELECT * FROM appointment WHERE atendido_appointment = 1";
                    $result=mysqli_query($conn,$sql) or die ('No se ejecuto la consulta');
                    $numerocita = 1;
                    $resultcheck=mysqli_num_rows($result);
                    while ($row=mysqli_fetch_array($result))
                    {
                        $id_cita = $row[0];
                        $fecha = $row[1];
                        $hora = $row[2];
                        $id_paciente = $row[3];
                        $atendido = $row[4];
                        $result_paciente = mysqli_query($conn,"SELECT * FROM patient WHERE id_patient = '$id_paciente'" );
                        $row_paciente=mysqli_fetch_array($result_paciente);
                        $nombre_paciente = $row_paciente[1];
                        $telefono_paciente = $row_paciente[4];                                                                                     
                        ?>
                    <tbody>
                        <tr style="background:  rgba(255, 255, 255, 0.534)">
                            <th scope="row" class="table-warning" style="font-size: 20px;background: rgba(241, 253, 76, 0.397)">
                                <?php echo $numerocita;?>
                            </th>
                            <td class="table-success" style="font-size: 20px;background: rgba(255, 255, 255, 0.397)">
                                <?php echo $fecha;?>
                            </td>
                            <td class="table-warning" style="font-size: 20px;background: rgba(255, 255, 255, 0.397)">
                                <?php echo $hora;?>
                            </td>
                            <td class="table-success" style="font-size: 20px;background: rgba(255, 255, 255, 0.397)">
                                <?php echo $nombre_paciente;?>
                            </td>
                            <td class="table-warning" style="font-size: 20px;background: rgba(255, 255, 255, 0.397)">
                                <?php echo $telefono_paciente;?>
                            </td>
                            <td class="table-success" style="font-size: 20px;background: rgba(255, 255, 255, 0.397)">                                
                                <a href="completar_cita.php?id_cita=<?php echo $id_cita ?>" class="btn btn-info" onClick="return confirm('Completar cita?')">Completada</a>                           
                            </td>
                        </tr>            
                        <?php $numerocita++;                    
                        } ?>
                    </tbody>
                </table>
                <?php } else{
                    echo 'No existen citas pendientes de confirmacion';
                }
                ?>
        </div>
    </form>
    <h2 class="font-italic">Historial de Citas</h2>
    <form method="POST" action="proceso.php">
        <div>
        <?php                        
                $sql= "SELECT * FROM appointment WHERE atendido_appointment = 2";
                $result=mysqli_query($conn,$sql) or die ('No se ejecuto la consulta');
                $numerocita = 1;
                $resultcheck=mysqli_num_rows($result);                
                if ($resultcheck>0) {                
                ?>
                <table class="table table-striped">
                    <thead>
                        <tr class="bg-warning" style="font-size: 20px;">
                            <th scope="col">#</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Hora</th>
                            <th scope="col">Paciente</th>
                            <th scope="col">Telefono</th>
                           
                        </tr>
                    </thead>
                    <?php                
                    require("../includes/conexion.php");                            
                    $sql= "SELECT * FROM appointment WHERE atendido_appointment = 2 ";
                    $result=mysqli_query($conn,$sql) or die ('No se ejecuto la consulta');
                    $numerocita = 1;
                    $resultcheck=mysqli_num_rows($result);
                    while ($row=mysqli_fetch_array($result))
                    {
                        $id_cita = $row[0];
                        $fecha = $row[1];
                        $hora = $row[2];
                        $id_paciente = $row[3];
                        $atendido = $row[4];
                        $result_paciente = mysqli_query($conn,"SELECT * FROM patient WHERE id_patient = '$id_paciente'" );
                        $row_paciente=mysqli_fetch_array($result_paciente);
                        $nombre_paciente = $row_paciente[1];
                        $telefono_paciente = $row_paciente[4];                                                                                     
                        ?>
                    <tbody>
                        <tr style="background:  rgba(255, 255, 255, 0.534)">
                            <th scope="row" class="table-warning" style="font-size: 20px;background: rgba(241, 253, 76, 0.397)">
                                <?php echo $numerocita;?>
                            </th>
                            <td class="table-success" style="font-size: 20px;background: rgba(255, 255, 255, 0.397)">
                                <?php echo $fecha;?>
                            </td>
                            <td class="table-warning" style="font-size: 20px;background:  rgba(255, 255, 255, 0.397)">
                                <?php echo $hora;?>
                            </td>
                            <td class="table-success" style="font-size: 20px;background:  rgba(255, 255, 255, 0.397)">
                                <?php echo $nombre_paciente;?>
                            </td>
                            <td class="table-warning" style="font-size: 20px;background:  rgba(255, 255, 255, 0.397)">
                                <?php echo $telefono_paciente;?>
                            </td>                           
                        </tr>            
                        <?php $numerocita++;                    
                        } ?>
                    </tbody>
                </table>
                <?php } else{
                    echo 'No existen registros de citas anteriores';
                }
                ?>
        </div>
    </form>
    <script>window.jQuery || document.write('<script src="../bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
    <script src="../bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
</body>
</html>

<?php }else {
header("Location: ../inicio.php");
}
?>