<?php
session_start();
if(isset($_SESSION['a_nombre'])){ ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Ingresar Receta Gratuita</title>
</head>

<body style="background: url(../imagenes/fondo_agregar_dieta.jpg)">

    <header>
        <nav class="navbar navbar-light" style="background-color: #4393cc;">
            <h2 class="navbar-brand">
                Añadir una receta gratuita
            </h2>
        </nav>
    </header>
    <form action="Guardarreceta.php" method="POST" style="color: white">
        <div class="container">
            <div style="background: rgba(0, 0, 0, 0.521);border-radius:20px;padding: 70px ">
                <h4 style="color: white">
                    Cada receta puede tener un maximo de 5 subtitulos
                </h4>
                <div class="form-group">
                    <input class="form-control" type="text" name="tituloreceta" placeholder="Titulo de la Receta" required>
                </div>
                <div id="div1" class="form-group row">
                    <div class="form-group col-md-3">
                        <input class="form-control " type="text" placeholder="Subtitulo 1" name="titulo1" id="t1" required>
                    </div>
                    <div class="form-group col-md-7">
                        <textarea class="form-control" name="contenido1" placeholder="Contenido..." id="c1"></textarea required>
                    </div>
                    <div class="form-group col-md-2">
                        <input class="btn btn-danger" type="button" value="Añadir titulo+subtitulo" id="mostrador1" required>
                    </div>
                </div>
                <div id="div2" class="form-group row">
                    <div class="form-group col-md-3">
                        <input class="form-control " type="text" placeholder="Subtitulo 2" name="titulo2" id="t2">
                    </div>
                    <div class="form-group col-md-7">
                        <textarea class="form-control" name="contenido2" placeholder="Contenido..." id="c2"></textarea>
                    </div>
                    <div class="form-group col-md-2">
                        <input class="btn btn-danger" type="button" value="Añadir titulo+subtitulo" id="mostrador2">
                    </div>
                </div>
                <div id="div3" class="form-group row">
                    <div class="form-group col-md-3">
                        <input class="form-control " type="text" placeholder="Subtitulo 3" name="titulo3" id="t3">
                    </div>
                    <div class="form-group col-md-7">
                        <textarea class="form-control" name="contenido3" placeholder="Contenido..." id="c3"></textarea>
                    </div>
                    <div class="form-group col-md-2">
                        <input class="btn btn-danger" type="button" value="Añadir titulo+subtitulo" id="mostrador3">
                    </div>
                </div>
                <div id="div4" class="form-group row">
                    <div class="form-group col-md-3">
                        <input class="form-control " type="text" placeholder="Subtitulo 4" name="titulo4" id="t4">
                    </div>
                    <div class="form-group col-md-7">
                        <textarea class="form-control" name="contenido4" placeholder="Contenido..." id="c4"></textarea>
                    </div>
                    <div class="form-group col-md-2">
                        <input class="btn btn-danger" type="button" value="Añadir titulo+subtitulo" id="mostrador4">
                    </div>
                </div>
                <div id="div5" class="form-group row">
                    <div class="form-group col-md-3">
                        <input class="form-control " type="text" placeholder="Subtitulo 5" name="titulo5" id="t5">
                    </div>
                    <div class="form-group col-md-7">
                        <textarea class="form-control" name="contenido5" placeholder="Contenido..." id="c5"></textarea>
                    </div>
                    <!-- <div class="form-group col-md-2">
                        <input class="btn btn-danger" type="button" value="Añadir titulo+subtitulo" id="mostrador1">
                    </div> -->
                </div>



                <!-- <div id="div2">
                    <label for="titulo2">Titulo 2</label>
                    <input type="text" name="titulo2" id="t2">
                    <label for="contenido2">Contenido 2</label>
                    <input type="text" name="contenido2" id="c2">
                    <input type="button" value="Añadir titulo+subtitulo" id="mostrador2">
                </div>
                <div id="div3">
                    <label for="">Titulo 3</label>
                    <input type="titulo3" name="titulo3" id="t3">
                    <label for="contenido3">Contenido 3</label>
                    <input type="text" name="contenido3" id="c3">
                    <input type="button" value="Añadir titulo+subtitulo" id="mostrador3">
                </div>
                <div id="div4">
                    <label for="">Titulo 4</label>
                    <input type="titulo4" name="titulo4" id="t4">
                    <label for="contenido4">Contenido 4</label>
                    <input type="text" name="contenido4" id="c4">
                    <input type="button" value="Añadir titulo+subtitulo" id="mostrador4">
                </div>
                <div id="div5">
                    <label for="titulo5">Titulo 5</label>
                    <input type="text" name="titulo5" id="t5">
                    <label for="contenido5">Contenido 5</label>
                    <input type="text" name="contenido5" id="c5">                    
                </div> -->
                <div class="form-group">
                    <input type="submit" name="Guardar2" value="Guardar" class="btn btn-success mx-auto d-block" style="font-size: 25px">
                </div>
            </div>
        </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#mostrador1").click(function () {
                $("#div2").show();
                $("#mostrador1").hide();
            });
            $("#mostrador2").click(function () {
                $("#div3").show();
                $("#mostrador2").hide();
            });
            $("#mostrador3").click(function () {
                $("#div4").show();
                $("#mostrador3").hide();
            });
            $("#mostrador4").click(function () {
                $("#div5").show();
                $("#mostrador4").hide();
            });
        });

        $(document).ready(function () {
            $("#div2").hide();
            $("#div3").hide();
            $("#div4").hide();
            $("#div5").hide();
        });
    </script>
</body>

</html>
<?php }else {
header("Location: ../inicio.php");
}
?>