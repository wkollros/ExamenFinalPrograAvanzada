<?php
session_start();
if(isset($_SESSION['a_nombre'])){
require("../includes/conexion.php"); 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Modificar un paciente</title>
</head>
<body>
    Aqui podra Modificar los datos de un paciente <br>
    <form method="POST" action="Guardar_receta_pat.php">
        
        <?php
        $sql = "SELECT * FROM patient"; 
        $query = mysqli_query($conn,$sql);
        echo '<select name="pacientes" size="1" id="selectpacientes" onchange="mostrarid()">';
        while ($row = mysqli_fetch_array($query)) {
        echo '<option value="' . $row['id_patient'] . '">' . $row['pat_name'] .' '.  $row['pat_last_name_1'] .' '. $row['pat_last_name_2'].'</option>';
        }
        echo '<option value="" disabled selected>Select your option</option>';
        echo '</select>';
        ?>
        <br>
        <input type="text" value="" id="id_paciente_selec">
        <input type="submit" value="Modificar datos" name="submit">
        <script>
            function mostrarid()
            {
                var e = document.getElementById("selectpacientes");
                var strUser = e.options[e.selectedIndex].value;
                alert("El id de paciente seleccionado es: "+strUser);
                document.getElementById("id_paciente_selec").value=strUser;
            }
            
        </script>
    </form>
</body>
</html>
<?php }else {
header("Location: ../inicio.php");
}
?>