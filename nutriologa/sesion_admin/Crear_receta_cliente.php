<?php 
session_start();
if(isset($_SESSION['a_nombre'])){
?>
<?php
require("../includes/conexion.php"); 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Enviar receta a un paciente</title>
</head>

<body style="background: url(../imagenes/fondo_enviar_receta.jpg)">

    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: #beb032;">
            <h1 class="navbar-brand">Asignar receta al paciente</h1>
        </nav>
    </header>
    <br>
    <br>
    <br>
    <form method="POST" action="Guardar_receta_pat.php">
        <div style="margin-top: 60px; background: rgba(0, 0, 0, 0.404);padding-top: 50px;padding-bottom: 50px;">
            <div class="container">
                <div class="form-group">
                    <h3 style="color:white">Eliga a un paciente y escriba la receta</h3>
                    <?php
                       $sql = "SELECT * FROM patient"; 
                       $query = mysqli_query($conn,$sql);
                       echo '<select name="pacientes" class="form-control" size="1" id="selectpacientes" style="width: 1000px; padding: 15px;">';
                       while ($row = mysqli_fetch_array($query)) { 
                       echo '<option class="form-control" value="' . $row['id_patient'] . '">' . $row['pat_name'] .' '.  $row['pat_last_name_1'] .' '. $row['pat_last_name_2'].'</option>';
                       }
                       echo '</select>';        
            ?>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="recetapaga" placeholder="Aqui va la receta del paciente elejido" style="width: 1000px;height: 306px;" required></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Crear receta" name="submit" class="btn btn-warning" style="font-size: 20px">
                </div>
            </div>
        </div>
    </form>

</body>

</html>

<?php }else {
header("Location: ../inicio.php");
}
?>