<?php
session_start();
?>
<?php 
if(isset($_SESSION['u_nombre'])){
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Citas</title>    
</head>

<body style="background: url(../imagenes/fondo_pedir_cita.jpg)">   
    <!-- <a href="http://localhost/nutriologa/sesion_iniciada/logeado.php">Volver</a> -->
    <form action="pedir_cita_fecha2.php" method="POST">
        <div style="margin: 150px; background-color: rgba(0, 0, 0, 0.39);padding: 20px;border-radius: 25px">
            <div class="container">
                <h2 style="color: white">Solicitar cita</h2>
                <!-- esto es para el nombre -->
                <!--vamos a poner los valores que ya sabemos que tenemos del usuario cuando se logea -->
                <div class="form-group">
                    <input required type="hidden" placeholder="id" class="form-control" name="id_paciente" value="<?php echo $_SESSION['u_id_pat'] ?>">
                </div>
                <!-- esto es para la fecha -->
                <div class="form-group">
                    <input type="date" placeholder="Fecha" class="form-control" name="fecha">
                </div>
                <!-- esto es para la hora -->
               <input type="submit" name="submit" value="Consultar">
        
    </form>    
</body>

</html>
<?php } else{
        header("Location: ../inicio.php"); 
    }
?>