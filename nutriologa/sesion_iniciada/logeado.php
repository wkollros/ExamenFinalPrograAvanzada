<?php
session_start();
require('../includes/conexion.php');
?>
<?php 
if(isset($_SESSION['u_nombre'])){
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Nutricionista</title>
    <link href="../bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../inicio.css" rel="stylesheet">
</head>

<body>

    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background-color: #4ebe32;">
            <h1 class="navbar-brand">Nutricionista</h1>
            <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="logeado.php">Inicio</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="nosotros" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Nosotros
                        </a>
                        <div class="dropdown-menu p-4 shadow-lg mb-5 rounded" style="width: 1000px; background: rgba(32, 133, 18, 0.699); color: white">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="../imagenes/nutriologa.jpg" alt="Perfil" style="width:240px" class="img-thumbnail">
                                </div>
                                <div class="col-md-9">
                                    <p class="lead font-weight-bold">
                                        Hola, mediante esta pagina te ayudare, brindandote informacion perzonalizada sobre dietas, recetas y como mejorar tu alimentación
                                        y ser una persona saludable...
                                    </p>
                                    <p class="lead font-weight-bold">
                                        Te invito a que te registres y podamos mejorar tu salud y bienestar.
                                    </p>
                                    <div class="dropdown-divider"></div>
                                    <h2 class="font-italic">
                                        Contactanos...
                                    </h2>
                                    <p class="lead font-weight-bold">
                                        79339707 - 4402534
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            Dietas
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#" id="adelgazar">Adelgazar 5 Kg</a>
                            <a class="dropdown-item" href="#" id="futbol">Dieta para futbolistas</a>
                            <a class="dropdown-item" href="#" id="gastritis">Gastritis</a>
                            <?php                               
                                $sql = "SELECT * FROM free_prescription" OR die ('No se ejecuto la consulta') ;
                                $res=mysqli_query($conn,$sql) ;
                                $rescheck=mysqli_num_rows($res);
                                if ($rescheck>0)
                                {
                                    while ($row=mysqli_fetch_array($res))
                                    {     
                                        $id_free_prescription = $row[0];           
                                        $titulo = $row[2];                                                      
                                        echo  '<a class="dropdown-item" href="../inicio_sin_login/Ver_Receta_Gratuita_especf.php?id_freepres='.$id_free_prescription.'" id="'.$id_free_prescription.'">'.$titulo.'</a>';
                                        // echo $titulo;
                                    }
                                }                               
                            ?> 
                            <!-- <a class="dropdown-item" href="#" id="gastritis">Gastritis</a>
                            <a class="dropdown-item" href="#" id="futbol">Dieta para futbolistas</a> -->
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://localhost/nutriologa/sesion_iniciada/PedirCita.php">Pedir Cita</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://localhost/nutriologa/sesion_iniciada/VerCitas.php">Ver Cita</a>
                    </li>                   
                    <li class="nav-item">
                        <a class="nav-link" href="http://localhost/nutriologa/sesion_iniciada/Ver_Recetas_pat.php">Ver Recetas Asignadas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://localhost/nutriologa/sesion_iniciada/enviar_correo.php">Contactanos</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="http://localhost/nutriologa/sesion_iniciada/pedir_cita_fecha.php">pedir cita 2</a>
                    </li> -->
                </ul>
                <?php 
                    if(isset($_SESSION['u_nombre'])){
                        echo '<form action="logout-inc.php" method="POST">            
                                 <button class="btn btn-info" type="submit" style="margin-left: 5px" name="submit">Cerrar Sesion</button>
                              </form>';                    
                   }else
                   {
                       echo '  
                           <form class="form-inline mt-2 mt-md-0" action="validar_login.php" method="POST">
                               <input type="text" name="correo" placeholder="Correo" class="form-control" style="margin-left: 5px">
                               <input type="text" name="password" placeholder="Password" class="form-control" style="margin-left: 5px">
                               <button class="btn btn-danger" type="submit" style="margin-left: 5px">Iniciar Sesion</button>
                               <a href="inicio_sin_login/registrar.html" class="btn btn-warning" style="margin-left: 5px">Registrate</a>
                           </form>';
                   }
                ?>
            </div>
        </nav>
    </header>

    <main role="main" class="color">

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="first-slide" src="../imagenes/foto1_slider.jpg" alt="First slide">
                    <div class="container">
                        <div class="carousel-caption text-left" style="background: rgba(0, 0, 0, 0.342);border-radius: 20px">
                            <b style="font-size: 30px">Aquellos que piensan que no tienen tiempo para una alimentación saludable tarde o temprano encontrarán
                                tiempo para la enfermedad.–Edward Stanley.</b>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="second-slide" src="../imagenes/foto2_slider.jpg" alt="Second slide">
                    <div class="container">
                        <div class="carousel-caption text-left" style="background: rgba(0, 0, 0, 0.342);border-radius: 20px">
                            <b style="font-size: 30px">Un hombre muy ocupado para cuidar de su salud es como un mecánico muy ocupado por cuidar sus
                                herramientas.
                            </b>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="third-slide" src="../imagenes/foto3_slider.jpg" alt="Third slide">
                    <div class="container">
                        <div class="carousel-caption text-left" style="background: rgba(0, 0, 0, 0.342);border-radius: 20px">
                            <h1>Come mas Saludable</h1>
                            <b style="font-size: 30px">Esforzarte para llevar una nutrición adecuada, es la mejor inversión para tu cuerpo y mente que
                                puedes hacer.</b>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

       <!-- esto es el contenido de inicio el sabias que -->
       <div id="contenido_inicio">
            <div class="container marketing">

                <hr class="featurette-divider">

                <div class="row featurette" style="background-color: rgb(253, 255, 209);border-radius: 50px">
                    <div class="col-md-7">
                        <h2 class="featurette-heading">¿SABIAS QUE?
                        </h2>
                        <p class="lead" style="font-size: 30px">Las piñas contribuyen a digerir mejor los alimentos, tienen efecto anti-inflamatorio y nos ayudan
                            a combatir la celulitis
                        </p>
                    </div>
                    <div class="col-md-5">
                        <img class="featurette-image img-fluid mx-auto rounded-circle border border-dark" src="../imagenes/p.jpg" alt="Generic placeholder image">
                    </div>
                </div>

                <hr class="featurette-divider">

                <div class="row featurette" style="background-color: rgb(253, 255, 209);border-radius: 50px">
                    <div class="col-md-7 order-md-2">
                        <h2 class="featurette-heading">¿SABIAS QUE?
                        </h2>
                        <p class="lead" style="font-size: 30px">Mezcla 250 gr. de avena cocida con tres cucharadas de leche y una de miel. Son proteinas de alto
                            valor nutricional con un gran sabor.
                        </p>
                    </div>
                    <div class="col-md-5 order-md-1">
                        <img class="featurette-image img-fluid mx-auto rounded-circle border border-dark" src="../imagenes/avena.jpg" alt="Generic placeholder image">
                    </div>
                </div>

                <hr class="featurette-divider">

                <div class="row featurette" style="background-color: rgb(253, 255, 209);border-radius: 50px">
                    <div class="col-md-7">
                        <h2 class="featurette-heading">¿SABIAS QUE?
                        </h2>
                        <p class="lead" style="font-size: 30px">La pechuga de pollo aporta mucha proteina sin grasa y pocas calorias. Preparala a la plancha. Aunque
                            no lo creas, es ideal para ingerir como snack.
                        </p>
                    </div>
                    <div class="col-md-5">
                        <img class="featurette-image img-fluid mx-auto rounded-circle border border-dark" src="../imagenes/pollo.jpg" alt="Generic placeholder image">
                    </div>
                </div>

                <hr class="featurette-divider">

            </div>
        </div>
        <!-- Aca termina el contenido inicio sabias que -->

         <!-- Aca inicia la dieta adelgazar 5 kg -->
         <div id="dieta_adelgazar">
            <div class="container">
                <div class="alert alert-success">
                    <h4>
                        Dieta para adelgazar 5 Kg.
                    </h4>
                    <p class="text-body" style="font-size: larger;">
                        Puede ser que quieras perder 5 kg por varios motivos: porque la ropa del verano pasado no te queda igual, porque tienes una
                        boda o un evento importante que se acerca, porque el médico te ha asustado con el resultado de las
                        últimas analíticas, por un propósito de año nuevo… o incluso porque llega el buen tiempo y quieres
                        perder esos kilitos que has ganado durante el invierno. Por todos esos motivos, son muchas las personas
                        que, para perder peso, se suman a la llamativa OPERACIÓN BIKINI y están dispuestas a seguir una dieta
                        milagro para conseguirlo. Pues bien, con este artículo voy a intentar que tú no seas una de ellas.
                    </p>
                    <h4>
                        Te pongo en contexto, ¿Qué es una dieta milagro?
                    </h4>
                    <p class="text-body" style="font-size: larger;">
                        Seguir dietas que presentan una o más de estas características: no tienen aval científico, presentan una restricción estricta
                        de según qué alimento (como los temibles hidratos de carbono, por ejemplo), se basan en un solo alimento:
                        en batidos, complementos que te prometen ser Elsa Pataki en 3 semanas… Te prometen rapidez en conseguir
                        los objetivos omitiendo las consecuencias que pueden conllevar, y un sinfín de ejemplos más. Aunque
                        esto último puede llamarte la atención, no te dejes llevar por la impaciencia. Cuando se siguen estas
                        recomendaciones, el objetivo en el 99% de los casos es puramente de pérdida de peso, sin preocuparse
                        por el mañana, por ese octubre que también llegará y si nos has seguido un proceso de educación nutricional,
                        todos los errores que hacías antes de empezar la operación bikini, volverán y la balanza volverá
                        a subir (conocido como el efecto yo-yo de las dietas no saludables). No tiene ningún sentido ponerse
                        a dieta cada mayo, para recuperarlo en septiembre y luego volver hacer operación bikini el año siguiente,
                        y así cada año… Es muy importante que tengas claro el objetivo y éste debe ser mejorar los hábitos
                        alimentarios y que estos cambios se mantengan para siempre. Debemos de querer más a nuestro cuerpo,
                        que al fin y al cabo sólo tenemos uno y debe durar para siempre. En definitiva, ninguna operación
                        es más eficaz que la que combina una alimentación saludable y variada durante los 365 días del año
                        y la práctica deportiva con continuidad.
                    </p>
                    <h4>
                        Perder 5 kilos en 3 días o en una semana
                    </h4>
                    <p class="text-body" style="font-size: larger;">
                        Internet y Youtube está lleno de vídeos con consejos para adelgazar 5 kilos en 3 días o en una semana. ¿Pero es posible perder
                        5 kilos en tan pocos días? La respuesta es que sí se puede hacer pero va en contra de tu salud. Si
                        buscas perder peso muy rápido, lo harás fundamentalmente en forma de agua y a costa de perder músculo.
                        Perder más de medio kilo de grasa a la semana es muy poco común, especialmente si no se lleva un
                        estilo de vida activo y se practica deporte. Para buscar este tipo de pérdidas tan rápidas, se suelen
                        hacer con dietas de sobres de menos de 1.000 calorías o mediante caldos o bebidas que aportan muy
                        pocas calorías (y nutrientes)
                        <br> Así que si te proponen perder peso muy rápido, que sepas que en cuanto vuelvas a comer normal cogerás
                        “al menos” estos kilos. ¡Y que tu salud y tu metabolismo no volverán a ser los mismos!
                        <br> Todo en lo que consiste la operación bikini, es lo que tienes que evitar si quieres perder peso
                        de forma saludable y, sobretodo, si quieres mantener esa pérdida de peso. Así que te voy a dar 12
                        claves para que puedas perder 5kg de forma saludable:
                        <br>
                        <strong>1.-No te saltes el desayuno:</strong> Intenta desayunar en cuando te levantes o al cabo de una hora
                        como máximo. Si no puedes con un gran desayuno, no pasa nada, puedes guardarte el bocadillo para
                        media mañana, pero al levantarte come algo (como por ejemplo un vaso de leche con copos de avena
                        o cereales sin azúcar añadido). No salgas de casa sin desayunar.
                        <br>
                        <strong>2.-En las comidas y las cenas, ten presente la Idea plato:</strong> Todas las comidas y las cenas
                        deben tener las proporciones adecuadas, es decir, si comes un plato: como parte principal la verdura
                        (sea cocida o ensalada), medio plato, por ejemplo. El plato ha de contener también una ración de
                        proteína, aproximadamente un cuarto del plato: carne, pescado, huevos o proteína vegetal (tofu, tempeh,
                        seitán, proteína de la soja texturizada…) y el otro cuarto del plato que nos falta serían los hidratos
                        (pasta, arroz, legumbre, patata, pan). Estos siempre será mejor consumirlos integrales. Con estas
                        referencias os estoy hablando de proporciones, no de cantidades, la cantidad variará según tus necesidades
                        y objetivos. Para perder esos 5 kg, ten en cuenta que la parte de hidratos de carbono debe ser una
                        cantidad más pequeña (os lo ejemplifico al final del artículo), pero que siempre deben estar presentes
                        tanto en las comidas como en las cenas.
                        <br>
                        <strong>3.-Come cada 3-4 horas:</strong> Si pasas muchas horas sin comer, te puede aparecer ansiedad y con
                        ello, muchas ganas de comer alimentos poco recomendables. El número de ingestas recomendadas varía
                        según tu horario, costumbres y estilo de vida, pero te recomiendo que tengas un orden en tu dieta
                        y que hagas una media mañana y una tarde saludable, y así evitarás llegar con más hambre a la comida
                        y la cena.
                        <br>
                        <strong>4.-No te saltes ninguna comida:</strong> Si lo haces a menudo con la finalidad de “compensar un extra”,
                        deja de hacerlo o sino tu cuerpo reaccionará y se va a defender. ¿Cómo? Tendrás más hambre en las
                        siguientes horas o incluso en el día siguiente y esto te hará comer más o picar. El hambre se acumula,
                        por lo tanto, saltarse comidas puede provocar el objetivo contrario de lo que buscas.
                        <br>
                        <strong>5.-Basa tu alimentación en alimentos saludables</strong> como pescado blanco, pescado azul (mínimo
                        1 día a la semana), fruta (2-3 piezas al día), verdura, legumbres, cereales integrales.
                        <br>
                        <strong>6.-Limita las grasas de tu dieta.</strong> Controla la cantidad de aceite de oliva (utilizado en
                        crudo y para cocinar) a 2-3 cucharadas soperas al día. Sobre todo, limita el consumo de alimentos
                        que te aporten grasas no saludables (embutidos, carnes procesadas o ahumadas, quesos light…). No
                        menosprecies las grasas procedentes de los alimentos light (pueden llegar a ser de un 30%). En su
                        lugar puedes utilizar grasas saludables como el aceite de oliva que te comentaba, aguacate, frutos
                        secos, pescado azul… Deja la carne roja para 1 día a la semana y elige carnes magras como el pollo
                        o el pavo.
                        <br>
                        <strong>7.-Evita los alimentos procesados, envasados:</strong> como las patatas chips, snacks, galletas…
                        En su lugar puedes hacer crudités de apio y zanahoria con hummus, brochetas de tomate cherry con
                        queso fresco, boquerones, pepinillos o frutos secos (siempre y cuando no estén fritos ni salados
                        y sea en pequeñas cantidades).
                        <br>
                        <strong>8.-Bebe 1’5 litros de agua diarios y evita alcohol, refrescos y zumos.</strong> La hidratación debes
                        realizarla básicamente con agua. Evita la bebidas azucaradas, refrescos, zumos envasados y alcohol,
                        que no dejan de ser un gran aporte de calorías y azúcares. Ocasionalmente podrías consumir un refresco
                        light, pero hay otras opciones más saludables, como un té frío, agua con gas…
                        <br>
                        <strong>9.-Organízate en la compra:</strong> Tener la nevera y la despensa llena de alimentos sanos es clave
                        para poder llevar a cabo una alimentación saludable. Planifica tus menús semanales y haz la compra
                        en consecuencia. Esto te ayudará a disminuir la improvisación y así evitar elecciones no deseadas.
                        <br>
                        <strong>10.-Utiliza cocciones sencillas y saludables:</strong> Las cocciones más adecuadas a utilizar son:
                        a la plancha o parrilla, horno, en su jugo, microondas, hervido, salteado, rehogado o al papillote.
                        Evita fritos y rebozados. Para darle sabor a tus platos, utiliza especias: tomillo, orégano, pimientas,
                        nuez moscada, cominos, etc. Y también ajo, cebolla, limón, perejil, mostaza, vinagre. Modera el consumo
                        de sal.
                        <br>
                        <strong>11.-Descansa/Duerme lo necesario:</strong> Según National Sleep Foundation (NSF), se recomienda a
                        los adultos de 26 a 64 años tener un rango de sueño recomendado: De 7 a 9 horas. Dormir menos de
                        lo recomendado dificulta la pérdida de peso, ya que aumenta el decaimiento y los picoteos.
                        <br><strong>12.-Practica ejercicio físico:</strong> No sólo es importante que tengas una vida activa: evitar el coche, los
                        ascensores, las escaleras mecánicas y el sofá. Mejor sube las escaleras a pie. ¡Y a parte encuentra
                        el deporte que te hace disfrutar! Puedes ir en bici, caminar, gimnasio… El ejercicio te ayuda a perder
                        grasa corporal, a mejorar tu forma física y tu salud. Si lo puedes practicar al aire libre mucho
                        mejor, así también aprovechas el contacto del sol con la piel y aumentas tu síntesis de vitamina
                        D.
                    </p>
                </div>
            </div>
        </div>
        <div id="dieta_futbol">
            <?php require('../includes/dieta1.html'); ?>
        </div>
        <div id="dieta_gastritis">
            <?php require('../includes/dieta2.html'); ?>
        </div>
        <footer style="background: url(../imagenes/footer.jpg); height: 400px;">
            <div style="background-color: rgba(0, 0, 0, 0.404); height: 400px">
                <p class="float-right">
                </p>
                <p>&copy; 2017-2018 Company, Inc. &middot;
                    <a href="#">Privacy</a> &middot;
                    <a href="#">Terms</a>
                </p>
            </div>
        </footer>
    </main>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#adelgazar").click(function () {
                $("#contenido_inicio").hide();
                $("#dieta_gastritis").hide();
                $("#dieta_futbol").hice();
                $("#dieta_adelgazar").show();

            });
            $("#futbol").click(function () {
                $("#contenido_inicio").hide();
                $("#dieta_adelgazar").hide();
                $("#dieta_futbol").show();
                $("#dieta_gastritis").hide();
            });
            $("#gastritis").click(function () {
                $("#contenido_inicio").hide();
                $("#dieta_adelgazar").hide();
                $("#dieta_futbol").hide();
                $("#dieta_gastritis").show();
            });
            // $("#show").click(function () {
            //     $("p").show();
            // });
        });
        // $(document).ready(function () {
        //     setTimeout(function () {
        //         $("#dieta_adelgazar").fadeOut(1500);
        //     }, 10);
        // });
        $(document).ready(function () {
            $("#dieta_adelgazar").hide();
            $("#dieta_futbol").hide();
            $("#dieta_gastritis").hide();
        });
    </script>
    </script>
    <script>window.jQuery || document.write('<script src="../bootstrap-4.1.1/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../bootstrap-4.1.1/assets/js/vendor/popper.min.js"></script>
    <script src="../bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
</body>

</html>
<?php } else{
        header("Location: ../inicio.php"); 
    }
?>