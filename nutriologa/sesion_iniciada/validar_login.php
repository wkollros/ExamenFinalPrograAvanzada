<?php
session_start();
    if(isset($_POST['submit'])){

        require("../includes/conexion.php");

        $username=mysqli_real_escape_string($conn, $_POST['correo']);
        $password=mysqli_real_escape_string($conn, $_POST['password']);  

        //Controlando errores
        //ver si dejaron espacios vacios
        if (empty($username) || empty($password)){
            header("Location: ../inicio.php?Login=Empty");
            exit();
            }else{
                    //primero veremos si es un admin
                
                $sql = "SELECT * FROM admin WHERE adm_email = '$username'";
                $result = mysqli_query($conn,$sql);
                $resultcheck= mysqli_num_rows($result);
                if ($resultcheck<1)
                {
                 //osea si no es admin q hacer? ver si es estudiante!
                 //codigo para ver si es un estudiante y demas
                    $sql = "SELECT * FROM patient WHERE pat_email = '$username'";
                    $result = mysqli_query($conn,$sql);
                    $resultcheck= mysqli_num_rows($result);
                    if ($resultcheck<1)
                    {
                        header("Location: ../inicio.php?Login=Noexisteusername");
                        exit();
                        //es mas seguro no decirle al usuario donde se equivoco para que no adivine valores tratando de entrar
                    }else{
                        if ($row = mysqli_fetch_assoc($result)){
                            if($password!=$row['pat_password'])
                            {
                                echo 'alert("Error en credenciales")';
                                header("Location: ../inicio.php?Login=errorcontraseña");                            
                                exit();
                            }else{

                                
                           
                                 //necesitamos usar este elseif para asegurarnos q no pase nada raro
                                //aqui logeamos al usuario
                                //para que funcione el $_SESSION debemos tener una sesion iniciada corriendo en nuestra pagina
                                $_SESSION['u_id_pat'] = $row['id_patient'];                                
                                $_SESSION['u_nombre'] = $row['pat_name'];
                                $_SESSION['u_ap_paterno'] = $row['pat_last_name_1'];
                                $_SESSION['u_ap_materno'] = $row['pat_last_name_2'];
                                $_SESSION['u_telefono'] = $row['pat_phone'];
                                $_SESSION['u_email'] = $row['pat_email'];
                                                                                                                               
                                header("Location: ../sesion_iniciada/logeado.php");
                                exit();
                            }
                        }
                    }               
                }else{
                    //entonces si SI es un admin q hacemos
                    if ($row = mysqli_fetch_assoc($result)){
                        //dehashing la contraseña                        
                        if($password!=$row['adm_password'])
                        {
                            header("Location: ../inicio.php?Login=errorcontraseña");                            
                            exit();
                        }else{      //necesitamos usar este elseif para asegurarnos q no pase nada raro
                            //aqui logeamos al usuario
                            //para que funcione el $_SESSION debemos tener una sesion iniciada corriendo en nuestra pagina                            
                            $_SESSION['a_nombre'] = $row['adm_name'];
                            $_SESSION['a_email'] = $row['adm_email'];
                            $_SESSION['a_last_name_1'] = $row['adm_last_name_1'];
                            $_SESSION['a_last_name_2'] = $row['adm_last_name_2'];
                            $_SESSION['a_id'] = $row['id_admin'];
                            header("Location: ../sesion_admin/logeado_admin.php");
                            exit();
                        }
                    }
                }            
            }
        }else
        {
            header("Location: ../inicio.php?Login=Error");
            exit();

        }
?>