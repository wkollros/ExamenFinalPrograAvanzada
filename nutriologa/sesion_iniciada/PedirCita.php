<?php
session_start(); 
if(isset($_SESSION['u_nombre'])){

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="pedir_cita.css">
    <title>Citas</title>
</head>

<body style="background: url(../imagenes/fondo_pedir_cita.jpg)">
    <!-- <a href="http://localhost/nutriologa/sesion_iniciada/logeado.php">Volver</a> -->
    <form action="guardarcita.php" method="POST" id="pedir_cita_form">
        <div style="margin: 150px; background-color: rgba(0, 0, 0, 0.39);padding: 20px;border-radius: 25px">
            <div class="container">
                <h2 style="color: white">Solicitar cita</h2>
                <h4 style="color: white">Las recervas se deben realizan para el dia siguinete en adelante</h4>
                <!-- esto es para el nombre -->
                <!--vamos a poner los valores que ya sabemos que tenemos del usuario cuando se logea -->
                <div class="form-group">
                    <input required type="hidden" placeholder="id" class="form-control" name="id_paciente" value="<?php echo $_SESSION['u_id_pat'] ?>">
                </div>
                <!-- esto es para la fecha -->
                <div class="form-group">
                    <input type="date" placeholder="Fecha" oninput="cambio(event)" class="form-control" name="fecha" id="fecha">
                </div>
                <!-- esto es para la hora -->
                <div class="form-group">
                    <input type="hidden" placeholder="Hora" class="form-control" name="hora" id="hora">
                </div>
                <br>
                <br>
                <br>
                <table class="table table-hover" id="tabla">
                    <thead>
                        <tr>
                            <th class="bg-success" scope="col">Horario</th>
                            <th class="bg-success" scope="col">Estado</th>
                            <th class="bg-success text-center" scope="col">Citas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="table-success" id="fila1">
                            <th scope="row">14:00</th>
                            <td>
                                <p id="hora1"></p>
                            </td>
                            <td class="text-center">
                                <button type="submit" class="btn btn-info" onclick="recuperar_hora('14:00')" id="boton1" disabled>Pedir Cita</button>
                            </td>
                        </tr>
                        <tr class="table-success" id="fila2">
                            <th scope="row">15:00</th>
                            <td>
                                <p id="hora2"></p>
                            </td>
                            <td class="text-center">
                                <button type="submit" class="btn btn-info" onclick="recuperar_hora('15:00')" id="boton2" disabled>Pedir Cita</button>
                            </td>
                        </tr>
                        <tr class="table-success" id="fila3">
                            <th scope="row">16:00</th>
                            <td>
                                <p id="hora3"></p>
                            </td>
                            <td class="text-center">
                                <button type="submit" class="btn btn-info" onclick="recuperar_hora('16:00')" id="boton3" disabled>Pedir Cita</button>
                            </td>
                        </tr>
                        <tr class="table-success" id="fila4">
                            <th scope="row">17:00</th>
                            <td>
                                <p id="hora4"></p>
                            </td>
                            <td class="text-center">
                                <button type="submit" class="btn btn-info" onclick="recuperar_hora('17:00')" id="boton4" disabled>Pedir Cita</button>
                            </td>
                        </tr>
                        <tr class="table-success" id="fila5">
                            <th scope="row">18:00</th>
                            <td>
                                <p id="hora5"></p>
                            </td>
                            <td class="text-center">
                                <button type="submit" class="btn btn-info" onclick="recuperar_hora('18:00')" id="boton5" disabled>Pedir Cita</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
    <input type="hidden" id="aux1" value="s">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        function recuperar_hora(hora) {
            document.getElementById("hora").value = hora;
        }
        $(document).ready(function () {
            $("#fecha").change(function () {
                var fecha = document.getElementById('fecha').value;
                realizaProceso(fecha);              
                // $(document).ready(function () {
                //     $("#fecha").hover(function () {
                //         descomponer();                                         
                //     });
                //     $("#tabla").hover(function () {
                //         descomponer();
                //     });                                    
                // });
            });
        });
        
        function cambio(event) {
                    descomponer();
                }
                
        function realizaProceso(fecha1) {
            var parametros = {
                "fecha": fecha1
            };
            $.ajax({
                data: parametros,
                url: 'recuperar_fecha.php',
                type: 'post',
                beforeSend: function () {
                    $("#aux").html("Procesando, espere por favor...");
                },
                success: function (response) {
                    document.getElementById("aux1").value = response;
                    if (document.getElementById('aux1').value == 'error'){                            
                            limpiar();                                                                          
                            alert("La fecha que escojio es invalida");
                            document.getElementById('aux1').value='';
                        }      
                        else{
                            descomponer(); 
                            }
                }
            });
        }
        function descomponer() {
            var horas = document.getElementById('aux1').value;
            var cadena = horas.split("@");
            var hora1, hora2, hora3, hora4, hora5;
            for (let i = 0; i <= 5; i++) {
                if (cadena[i] == '14:00:00') {
                    hora1 = 1;
                }
                if (cadena[i] == '15:00:00') {
                    hora2 = 1;
                }
                if (cadena[i] == '16:00:00') {
                    hora3 = 1;
                }
                if (cadena[i] == '17:00:00') {
                    hora4 = 1;
                }
                if (cadena[i] == '18:00:00') {
                    hora5 = 1;
                }
            }
            if (hora1 == 1) {
                document.getElementById('hora1').innerHTML = 'Ocupado';
                $(document).ready(function () {
                    $("#fila1").removeClass("table-success");
                    $("#fila1").addClass("table-danger");
                    $('#boton1').attr("disabled", true);
                });
            } else {
                document.getElementById('hora1').innerHTML = 'Disponible';
                $(document).ready(function () {
                    $("#fila1").removeClass("table-danger");
                    $("#fila1").addClass("table-success");
                    $('#boton1').attr("disabled", false);
                });
            }
            if (hora2 == 1) {
                document.getElementById('hora2').innerHTML = 'Ocupado';
                $(document).ready(function () {
                    $("#fila2").removeClass("table-success");
                    $("#fila2").addClass("table-danger");
                    $('#boton2').attr("disabled", true);
                });
            } else {
                document.getElementById('hora2').innerHTML = 'Disponible';
                $(document).ready(function () {
                    $("#fila2").removeClass("table-danger");
                    $("#fila2").addClass("table-success");
                    $('#boton2').attr("disabled", false);
                });
            }
            if (hora3 == 1) {
                document.getElementById('hora3').innerHTML = 'Ocupado';
                $(document).ready(function () {
                    $("#fila3").removeClass("table-success");
                    $("#fila3").addClass("table-danger");
                    $('#boton3').attr("disabled", true);
                });
            } else {
                document.getElementById('hora3').innerHTML = 'Disponible';
                $(document).ready(function () {
                    $("#fila3").removeClass("table-danger");
                    $("#fila3").addClass("table-success");
                    $('#boton3').attr("disabled", false);
                });
            }
            if (hora4 == 1) {
                document.getElementById('hora4').innerHTML = 'Ocupado';
                $(document).ready(function () {
                    $("#fila4").removeClass("table-success");
                    $("#fila4").addClass("table-danger");
                    $('#boton4').attr("disabled", true);
                });
            } else {
                document.getElementById('hora4').innerHTML = 'Disponible';
                $(document).ready(function () {
                    $("#fila4").removeClass("table-danger");
                    $("#fila4").addClass("table-success");
                    $('#boton4').attr("disabled", false);
                });
            }
            if (hora5 == 1) {
                document.getElementById('hora5').innerHTML = 'Ocupado';
                $(document).ready(function () {
                    $("#fila5").removeClass("table-success");
                    $("#fila5").addClass("table-danger");
                    $('#boton5').attr("disabled", true);
                });
            } else {
                document.getElementById('hora5').innerHTML = 'Disponible';
                $(document).ready(function () {
                    $("#fila5").removeClass("table-danger");
                    $("#fila5").addClass("table-success");
                    $('#boton5').attr("disabled", false);
                });
            }
        }
        function  limpiar () {
            document.getElementById('hora1').innerHTML = 'Ocupado';
            document.getElementById('hora2').innerHTML = 'Ocupado';
            document.getElementById('hora3').innerHTML = 'Ocupado';
            document.getElementById('hora4').innerHTML = 'Ocupado';
            document.getElementById('hora5').innerHTML = 'Ocupado';
                $(document).ready(function () {
                    $("#fila1").removeClass("table-success");
                    $("#fila1").addClass("table-danger");
                    $('#boton1').attr("disabled", true);
                    $("#fila2").removeClass("table-success");
                    $("#fila2").addClass("table-danger");
                    $('#boton2').attr("disabled", true);
                    $("#fila3").removeClass("table-success");
                    $("#fila3").addClass("table-danger");
                    $('#boton3').attr("disabled", true);
                    $("#fila4").removeClass("table-success");
                    $("#fila4").addClass("table-danger");
                    $('#boton4').attr("disabled", true);
                    $("#fila5").removeClass("table-success");
                    $("#fila5").addClass("table-danger");
                    $('#boton5').attr("disabled", true);
                    
                });
        }        
    </script>
</body>

</html>
<?php } else{
        header("Location: ../inicio.php"); 
    }
?>