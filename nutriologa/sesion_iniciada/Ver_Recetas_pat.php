<?php
session_start();
?>
<?php 
if(isset($_SESSION['u_nombre'])){
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../bootstrap-4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="ver_receta.css">
    <title>Recetas Asignadas</title>
</head>

<body>
    <nav class="navbar navbar-light" style="background-color: #faf744c9;">
        <h3>
            Receta asignada al paciente:
            <?php  echo $_SESSION['u_nombre'] . ' ' .$_SESSION['u_ap_paterno'] . ' ' . $_SESSION['u_ap_materno']  ;?>
        </h3>
    </nav>

    <?php               
        require("../includes/conexion.php");     
        $id_paciente=$_SESSION['u_id_pat'];       
        $sql="SELECT * FROM pat_prescription WHERE id_patient = '$id_paciente' order by date_pat_prescription DESC";
        $res=mysqli_query($conn,$sql);
        $reschek=mysqli_num_rows($res);
        if ($reschek>0) {
            ?>
    <?php
        while ($row=mysqli_fetch_array($res))
        {
            $receta=$row[1];
            $fecha=$row[3];
    ?>
    <div class="container" style="background: rgba(0, 0, 0, 0.438);border-radius: 20px;margin-top: 40px">
        <p class="font-weight-bold" style="color: white;font-size: 25px">
            <?php echo 'Receta de la fecha: '.$fecha.'<br>';?>
        </p>
        <p class="font-weight-bold" style="color: white;font-size: 22px;padding-bottom: 10px">
            <?php echo $receta. '<br>';?>
        </p>
    </div>


    <?php        
        }        
    ?>
    <?php }else{
       ?>
    <div class="container" style="background: rgba(0, 0, 0, 0.438);border-radius: 20px;margin-top: 40px">
        <p class="font-weight-bold" style="color: white;font-size: 25px">
            <?php  echo 'No tienes Recetas asignadas... :( ';?>
        </p>
    </div>

    <?php
    } 
    
    ?>



</body>

</html>
<?php } else{
        header("Location: ../inicio.php"); 
    }
?>